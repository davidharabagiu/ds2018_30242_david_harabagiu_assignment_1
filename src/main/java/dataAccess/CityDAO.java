package dataAccess;

import models.City;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class CityDAO {

    private SessionFactory factory;

    public CityDAO(SessionFactory factory) {
        this.factory = factory;
    }

    @SuppressWarnings("unchecked")
    public City getCity(String name) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE name = :name");
            query.setParameter("name", name);
            cities = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }
}
