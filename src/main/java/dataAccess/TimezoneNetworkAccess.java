package dataAccess;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class TimezoneNetworkAccess {

    public String getLocalTime(double latitude, double longitude) throws Exception {
        String url = "http://www.new.earthtools.org/timezone-1.1/" + latitude + "/" + longitude;

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        int responseCode = con.getResponseCode();
        if (responseCode != 200) {
            return "";
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        String xmlData = response.toString();

        SAXBuilder saxBuilder = new SAXBuilder();
        InputStream inputStream = new ByteArrayInputStream(xmlData.getBytes());
        Document document = saxBuilder.build(inputStream);
        Element rootNode = document.getRootElement();
        return rootNode.getChildText("localtime");
    }
}
