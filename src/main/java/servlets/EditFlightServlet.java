package servlets;

import services.FlightsService;
import models.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;

public class EditFlightServlet extends HttpServlet {

    private static final String FLIGHT_NUMBER_PARAM = "number";

    private FlightsService flightsService;

    public void init() throws ServletException {
        flightsService = new FlightsService();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        boolean isAdmin = false;
        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals("admin") && c.getValue().equals("true")) {
                    isAdmin = true;
                    break;
                }
            }
        }
        if (!isAdmin) {
            out.println("<h1>Access denied</h1>");
            return;
        }

        String flightNumber = request.getParameter(FLIGHT_NUMBER_PARAM);
        Flight flight = flightsService.getFlight(flightNumber);

        out.println("<h1>Edit Flight</h1>");
        out.println("<form action=\"flight\" method=\"POST\">");
        out.println("Flight Number: <input readonly type=\"text\" name=\"number\" value=\"" + flight.getNumber() + "\"><br/>");
        out.println("Airplane Type: <input type=\"text\" name=\"airplane_type\" value=\"" + flight.getAirplaneType() + "\"><br/>");
        out.println("Departure City: <input type=\"text\" name=\"departure_city\" value=\"" + flight.getDepartureCity().getName() + "\"><br/>");
        out.println("Departure Date: <input type=\"text\" name=\"departure_date\" value=\"" + flight.getDepartureDate() + "\"><br/>");
        out.println("Departure Time: <input type=\"text\" name=\"departure_time\" value=\"" + flight.getDepartureTime() + "\"><br/>");
        out.println("Arrival City: <input type=\"text\" name=\"arrival_city\" value=\"" + flight.getArrivalCity().getName() + "\"><br/>");
        out.println("Arrival Date: <input type=\"text\" name=\"arrival_date\" value=\"" + flight.getArrivalDate() + "\"><br/>");
        out.println("Arrival Time: <input type=\"text\" name=\"arrival_time\" value=\"" + flight.getArrivalTime() + "\"><br/>");
        out.println("<input type = \"submit\" value = \"Edit\"></form>");
        out.println("<form action=\"deleteflight\" method=\"POST\">");
        out.println("<input hidden type=\"text\" name=\"number\" value=\"" + flight.getNumber() + "\">");
        out.println("<input type = \"submit\" value = \"Delete\"><br/></form>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isAdmin = false;
        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals("admin") && c.getValue().equals("true")) {
                    isAdmin = true;
                    break;
                }
            }
        }
        if (!isAdmin) {
            doGet(request, response);
        }

        String flightNumber = request.getParameter(FLIGHT_NUMBER_PARAM);
        Flight flight = flightsService.getFlight(flightNumber);

        flight.setAirplaneType(request.getParameter("airplane_type"));
        flight.setDepartureCity(flightsService.getCity(request.getParameter("departure_city")));
        flight.setDepartureDate(Date.valueOf(request.getParameter("departure_date")));
        flight.setDepartureTime(Time.valueOf(request.getParameter("departure_time")));
        flight.setArrivalCity(flightsService.getCity(request.getParameter("arrival_city")));
        flight.setArrivalDate(Date.valueOf(request.getParameter("arrival_date")));
        flight.setArrivalTime(Time.valueOf(request.getParameter("arrival_time")));

        flightsService.editFlight(flight);

        doGet(request, response);
    }

    public void destroy() { }
}
