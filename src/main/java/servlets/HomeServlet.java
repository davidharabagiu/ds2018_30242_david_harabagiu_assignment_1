package servlets;

import services.FlightsService;
import services.TimezoneService;
import models.City;
import models.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class HomeServlet extends HttpServlet {

    private FlightsService flightsService;

    public void init() throws ServletException {
        flightsService = new FlightsService();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (request.getCookies() == null) {
            out.println("<h1>Login failed</h1>");
            return;
        }

        boolean loggedIn = false;
        boolean isAdmin = false;

        for (Cookie c : request.getCookies()) {
            if (c.getName().equals("session_token") && c.getValue().equals("123")) {
                loggedIn = true;
                break;
            }
        }

        if (!loggedIn) {
            out.println("<h1>Login failed</h1>");
            return;
        }

        String userName = "";
        for (Cookie c : request.getCookies()) {
            if (c.getName().equals("username")) {
                userName = c.getValue();
                break;
            }
        }

        for (Cookie c : request.getCookies()) {
            if (c.getName().equals("admin") && c.getValue().equals("true")) {
                isAdmin = true;
                break;
            }
        }

        out.println("<h1>Welcome, " + userName + "!</h1>");

        out.println(getAllFlights(isAdmin));

        if (isAdmin) {
            out.println("<form action=\"createflight\" method=\"GET\">");
            out.println("<input type = \"submit\" value = \"Create Flight\"><br/></form>");
        }
    }

    private String getAllFlights(boolean isAdmin) {
        StringBuilder resBuilder = new StringBuilder();
        resBuilder.append("<style>table, th, td {border: 1px solid black;}td{text-align: center;}</style>");
        resBuilder.append("<table style=\"width:100%\"><tr><th>Flight Number</th><th>Airplane Type</th><th>" +
                "Departure City</th><th>Departure Time</th><th>Arrival City</th><th>Arrival Time</th></tr>");
        List<Flight> flightList = flightsService.getFlights();
        Set<City> cities = new TreeSet<City>(new Comparator<City>() {
            public int compare(City o1, City o2) {
                return o1.getId() - o2.getId();
            }
        });
        for (Flight f : flightList) {
            resBuilder.append("<tr><td>");
            if (isAdmin) {
                resBuilder.append("<a href=\"flight?number=" + f.getNumber() + "\">");
            }
            resBuilder.append(f.getNumber());
            if (isAdmin) {
                resBuilder.append("</a>");
            }
            resBuilder.append("</td><td>" + f.getAirplaneType() + "</td><td>" +
                    f.getDepartureCity().getName() + "</td><td>" + f.getDepartureDate() + " " + f.getDepartureTime() +
                    "</td><td>" + f.getArrivalCity().getName() + "</td><td>" + f.getArrivalDate() + " " +
                    f.getArrivalTime() + "</td></tr>");
            cities.add(f.getDepartureCity());
            cities.add(f.getArrivalCity());
        }
        resBuilder.append("</table><br/>");
        resBuilder.append("<table style=\"width:40%\"><tr><th>City</th><th>Local Time</th></tr>");
        TimezoneService timezoneService = new TimezoneService();
        for (City c : cities) {
            resBuilder.append("<tr><td>" + c.getName() + "</td><td>" +
                    timezoneService.getLocalTime(c.getLatitude(), c.getLongitude()) + "</td></tr>");
        }
        resBuilder.append("</table>");
        return resBuilder.toString();
    }

    public void destroy() { }
}
