package servlets;

import services.FlightsService;
import models.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DeleteFlightServlet extends HttpServlet {


    private FlightsService flightsService;

    public void init() throws ServletException {
        flightsService = new FlightsService();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Flight deleted successfully");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        boolean isAdmin = false;
        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals("admin") && c.getValue().equals("true")) {
                    isAdmin = true;
                    break;
                }
            }
        }
        if (!isAdmin) {
            out.println("Access denied");
            return;
        }

        String flightNumber = request.getParameter("number");
        Flight flight = flightsService.getFlight(flightNumber);

        flightsService.deleteFlight(flight);

        doGet(request, response);
    }

    public void destroy() { }
}
