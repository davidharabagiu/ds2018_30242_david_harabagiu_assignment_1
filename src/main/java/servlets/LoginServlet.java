package servlets;

import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    private UserService userService;

    public void init() throws ServletException {
        userService = new UserService();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("username");
        String password = request.getParameter("password");

        PrintWriter out = response.getWriter();

        if (!userService.userExists(userName, password)) {
            out.println("<h1>Login failed</h1>");
            return;
        }

        response.addCookie(new Cookie("session_token", "123"));
        response.addCookie(new Cookie("username", userName));

        boolean isAdmin = userService.isAdmin(userName, password);

        if (isAdmin) {
            response.addCookie(new Cookie("admin", "true"));
        }

        response.sendRedirect("home");
    }

    public void destroy() { }
}
