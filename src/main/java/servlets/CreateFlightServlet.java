package servlets;

import services.FlightsService;
import models.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;

public class CreateFlightServlet extends HttpServlet {

    private FlightsService flightsService;

    public void init() throws ServletException {
        flightsService = new FlightsService();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        boolean isAdmin = false;
        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals("admin") && c.getValue().equals("true")) {
                    isAdmin = true;
                    break;
                }
            }
        }
        if (!isAdmin) {
            out.println("<h1>Access denied</h1>");
            return;
        }

        out.println("<h1>Create Flight</h1>");
        out.println("<form action=\"createflight\" method=\"POST\">");
        out.println("Flight Number: <input type=\"text\" name=\"number\"><br/>");
        out.println("Airplane Type: <input type=\"text\" name=\"airplane_type\"><br/>");
        out.println("Departure City: <input type=\"text\" name=\"departure_city\"><br/>");
        out.println("Departure Date: <input type=\"text\" name=\"departure_date\"><br/>");
        out.println("Departure Time: <input type=\"text\" name=\"departure_time\"><br/>");
        out.println("Arrival City: <input type=\"text\" name=\"arrival_city\"><br/>");
        out.println("Arrival Date: <input type=\"text\" name=\"arrival_date\"><br/>");
        out.println("Arrival Time: <input type=\"text\" name=\"arrival_time\"><br/>");
        out.println("<input type = \"submit\" value = \"Create\"></form>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isAdmin = false;
        if (request.getCookies() != null) {
            for (Cookie c : request.getCookies()) {
                if (c.getName().equals("admin") && c.getValue().equals("true")) {
                    isAdmin = true;
                    break;
                }
            }
        }
        if (!isAdmin) {
            doGet(request, response);
        }

        Flight flight = new Flight();

        flight.setNumber(request.getParameter("number"));
        flight.setAirplaneType(request.getParameter("airplane_type"));
        flight.setDepartureCity(flightsService.getCity(request.getParameter("departure_city")));
        flight.setDepartureDate(Date.valueOf(request.getParameter("departure_date")));
        flight.setDepartureTime(Time.valueOf(request.getParameter("departure_time")));
        flight.setArrivalCity(flightsService.getCity(request.getParameter("arrival_city")));
        flight.setArrivalDate(Date.valueOf(request.getParameter("arrival_date")));
        flight.setArrivalTime(Time.valueOf(request.getParameter("arrival_time")));

        flightsService.createFlight(flight);

        PrintWriter out = response.getWriter();
        out.println("Flight created successfully");
    }

    public void destroy() { }
}
