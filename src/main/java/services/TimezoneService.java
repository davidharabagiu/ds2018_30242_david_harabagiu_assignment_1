package services;

import dataAccess.TimezoneNetworkAccess;

public class TimezoneService {

    public String getLocalTime(double latitude, double longitude) {
        try {
            return new TimezoneNetworkAccess().getLocalTime(latitude, longitude);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "ERROR";
    }
}
