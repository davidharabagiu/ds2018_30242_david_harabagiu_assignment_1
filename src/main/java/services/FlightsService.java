package services;

import dataAccess.CityDAO;
import dataAccess.FlightDAO;
import models.City;
import models.Flight;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class FlightsService {

    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public FlightsService() {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        flightDAO = new FlightDAO(sessionFactory);
        cityDAO = new CityDAO(sessionFactory);
    }

    public List<Flight> getFlights() {
        return flightDAO.getFlights();
    }

    public Flight getFlight(String flightNumber) {
        return flightDAO.getFlight(flightNumber);
    }

    public void createFlight(Flight flight) {
        flightDAO.createFlight(flight);
    }

    public void editFlight(Flight flight) {
        flightDAO.updateFlight(flight);
    }

    public void deleteFlight(Flight flight) {
        flightDAO.deleteFlight(flight);
    }

    public City getCity(String name) {
        return cityDAO.getCity(name);
    }
}
