package services;

import dataAccess.UserDAO;
import models.User;
import org.hibernate.cfg.Configuration;

public class UserService {

    private UserDAO userDAO;

    public UserService() {
        userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
    }

    public boolean userExists(String name, String password) {
        return userDAO.findUser(name, password) != null;
    }

    public boolean isAdmin(String name, String password) {
        User user = userDAO.findUser(name, password);
        return user != null && user.getIsAdmin();
    }
}
